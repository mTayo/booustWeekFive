<?php
	require_once 'classes/Exception.php';
	require_once 'classes/PHPMailer.php';
	require_once 'classes/SMTP.php';
	class formProcess{

		public function prop($myname,$myemail,$mymsg){
			$this->name=$myname;
			$this->email=$myemail;
			$this->msg=$mymsg;
			
		}
		public function saveincsv() {
			$opencsv= fopen("contactus.csv", "a");
			$rowNos= count(file("contactus.csv"));
			if ($rowNos > 1) {
				$rowNos=($rowNos - 1) + 1;
			}
			$savedata = array(
				'S/N' => $rowNos,
				'Name' => $this->name,
				'Email Add' =>$this->email,
				'Message' =>$this->msg,
				'Date'=> date("Y-m-d H:i:s")

			);
			fputcsv($opencsv, $savedata);
			
		}
		public function sendMail(){
				$mail = new PHPMailer\PHPMailer\PHPMailer();

				$mail->IsSMTP();
				$mail->Mailer = 'smtp';
				$mail->SMTPAuth = true;
				$mail->Host = 'smtp.gmail.com'; 
				$mail->Port = 465;
				$mail->SMTPSecure = 'ssl';
				$mail->Username = "moviecafe1@gmail.com";
				$mail->Password = "123450xyz!";
				 
				$mail->IsHTML(true); 
				$mail->SingleTo = true; 
				 
				$mail->From = "moviecafe1@gmail.com";
				$mail->FromName = "Moviecafe";
				 
				$mail->addAddress($this->email);
				 
				$mail->Subject = "Testing Moviecafe";
				$mail->Body = $this->msg;
				try{
					$mail->Send();
					if(!$mail->send()){
						throw new Exception("Error Processing Request");
					}
					header('location: '.$_SERVER['HTTP_REFERER'].'?status=success');
				}
				catch(Exception $e){
				    error_log($e->getMessage(),0);
				    header('location: '. $_SERVER['HTTP_REFERER'].'?status=error');
				}
		}
		public function propTwo($myname,$myemail){
			$this->name=$myname;
			$this->email=$myemail;	
		}
		public function newslettersave(){
			$newscsv= fopen("newsletter.csv", "a");
			$newsNos= count(file("newsletter.csv"));
			if ($newsNos > 1) {
				$newsNos=($newsNos - 1) + 1;
			}
			$savedata = array(
				'S/N' => $newsNos,
				'Name' => $this->name,
				'Email Add' =>$this->email,
				'Message' =>$this->msg,
				'Date'=> date("Y-m-d H:i:s")

			);
			fputcsv($newscsv, $savedata);
			
		}
		public function validateEmail($mail){
			if (!filter_var($mail,FILTER_VALIDATE_EMAIL)) {
				return false;
			}
			
			return true;
		} 

		public function validateName($param){
				if(!preg_match("/[a-zA-Z'-]/",$param)){
					return false;
				}
				return true;
		}
		public function validateMessage($mesage){
				$mesage=trim($mesage);
				if (strlen($mesage)<=2) {
					return false;
				}
				return true;
		}
	}


 	

 
?>
