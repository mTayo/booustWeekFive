<?php
	require('vendor/autoload.php');	
class dbObj{
  		private $db_url;
		private $db_dsn;
		private $db_host;
		private $db_user;
		private $db_pass;	
		private $db_name;	
		private $db_conn;

		
		function __construct(){
			$this->db_url  = parse_url(getenv("CLEARDB_DATABASE_URL")); 
			$this->db_host = isset($this->db_url['host'])? $this->db_url['host'] : 'localhost';
			$this->db_user = isset($this->db_url['user'])? $this->db_url['user'] : 'root';
			$this->db_pass = isset($this->db_url['pass'])?$this->db_url['pass'] : '';
			$this->db_name = strlen($this->db_url['path']) > 0 ? substr($this->db_url["path"], 1) : 'dbase'; 
			$this->db_dsn  = 'mysql:host='.$this->db_host.'; dbname='.$this->db_name; 
			$this->db_conn = $this->dbConnect();
			
		}
		function dbConnect(){
			
       		try{
                $connect = new PDO($this->db_dsn, $this->db_user,$this->db_pass);
                $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }

            catch(PDPException $e){
                die ("connection failed:". $e->getMessage());
            }
            return $connect;
		}
		function get_video($mail)
	{
		
		//$mail=$_SESSION['user'];
		$sql="SELECT * FROM video WHERE email=? ORDER BY ID DESC LIMIT 4";
		$stmt=$this->db_conn->prepare($sql);
		$stmt->bindParam(':email', $mail);
		$stmt->execute([$mail]);
		
		while($row = $stmt->fetch(PDO::FETCH_ASSOC))
		   {
		    $data[] = $row;
		   }
		   return $data;
		  }
		  function alter(){
			// try{
			// 	$sql="ALTER TABLE  video ADD rating int(10) NOT NULL AFTER location";
			// 	$this->db_conn->exec($sql);
			// }
			// catch(PDOException $e){
			// 			echo $e->getMessage();
			// 		}
		  }
		  function delete_video($id){
		  		$status = $body = '';
		  		
		  		$query="DELETE FROM video WHERE id=? ";
				$statement=$this->db_conn->prepare($query);
				$statement->bindParam(':id',$id);
				if($statement->execute([$id])){
					$status = 'success';
					$body   = 'upload successful';
				}else{
				$status = 'error';
				$body = 'an error occured please try again!';
			}

/*			
		}else{

			$status = 'error';
			$body   = 'Unsupported File Format';
		}*/
		return ['status'=>$status,'body'=>$body];
					

		  }
	
	function insert_video($post_files,$file_upload)
	{
		$name=$file_upload['file']['name'];
        
		$status = $body = '';
		
		$validate_request = true;
		// if (!preg_match('/ video\* /',$file_upload['file']['type'])) {
		// 	$valldate_request = false;
		// }

		//if ($valldate_request) {
			$path= 'https://cinemacafe.s3.amazonaws.com/';
			$location=$path.$name;

			$s3 = Aws\S3\S3Client::factory();
			$bucket = getenv('S3_BUCKET')?: 'cinemacafe';
			
			$sql = "INSERT INTO video (description,location,rating,email) VALUES(:description,:location,:rating,:email)";
			$stmt=$this->db_conn->prepare($sql);
			$stmt->bindParam(':description',$post_files['description']);
			 $stmt->bindParam(':location',$location);
			$stmt->bindParam('rating',$post_files['rating']);
			//var_dump($post_files['email']);exit();
			$stmt->bindParam(':email',$post_files['email']);	
			
           
            
            if ($stmt->execute()) {
            	$upload = $s3->upload($bucket, $name, fopen($file_upload['file']['tmp_name'], 'rb'), 'public-read');
				$status = 'success';
				$body   = 'upload successful';	
			}else{
				$status = 'error';
				$body = 'an error occured please try again!';
			}

/*			
		}else{

			$status = 'error';
			$body   = 'Unsupported File Format';
		}*/
		return ['status'=>$status,'body'=>$body];
	}

}








// if($_GET["action"] == 'insert')
// {
//  $data = $api_object->insert();
// }


//echo json_encode($data);


?>