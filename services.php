<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8">
		  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		  	<meta http-equiv="X-UA-Compatible" content="ie=edge">
			<title>
				Services Movie Cafe Alpha
			</title>
			<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
			<link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/bootstrap.min.css">
			<link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/style.css">
			<script type="text/javascript" src="bootstrap-4.0.0-beta/js/jquery-3.2.1.min.js"></script>
			<script type="text/javascript" src="bootstrap-4.0.0-beta/js/popper.min.js"></script>
			<script type="text/javascript" src="bootstrap-4.0.0-beta/js/bootstrap.min.js"></script>
		</head>
		<body>
			<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
				<a class="navbar-brand" href="index.php">Movie Cafe</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item">
							<a class="nav-link" href="index.php">Home</a>
						</li>
						<li class="nav-item dropdown" >
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" data-toggle="dropdown">Lastest movies</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="nigerianMovies.html">Nigerian Movies</a>
								<a class="dropdown-item" href="americanMovies.html">American movies/Series</a>
								<a class="dropdown-item" href="asian.html">Asian Movies</a>
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="boxoffice.html">Box Office</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="services.html">Services</a>
						</li>
					</ul>
					 <form class="form-inline my-2 my-lg-0">
					      <input class="form-control mr-sm-2" type="search" placeholder="Search for a Movie" aria-label="Search">
					      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search </button>
				    </form>
				</div>
			</nav>
				
			<div style="background:rgba(0,0,0,.5);  height: 24vh; width:100%; " class="d-flex justify-content-center flex-column text-white mt-5">
				<h1 class=" display-4 mx-auto">Services</h1>
			</div>
			<div class="container"  style="height: 45vh;">
				<p class="lead text-center mt-5" style="font-size: 300%;">Currently Unavailable</p>
			</div>
			<footer class="page-footer font-small cyan darken-3 text-center ">
		    	<div class="container">
			        <div class="row">
			        	<div class="col-md-12 py-5">
			          		<div class="mb-5 flex-center">
			            		<a class="fb-ic">
			              			<i class="fa fa-facebook fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
			            		</a>
			            		<a class="tw-ic">
			              			<i class="fa fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
			            		</a>
			            		<a class="gplus-ic">
			              			<i class="fa fa-google-plus fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
			            		</a>
					            <a class="li-ic">
					              	<i class="fa fa-linkedin fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
					            </a>
					            <a class="ins-ic">
					              	<i class="fa fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
					            </a>
					            <a class="pin-ic">
					              	<i class="fa fa-pinterest fa-lg white-text fa-2x"> </i>
					            </a>
			          		</div>
			       		</div>
			        </div>
			    </div>
			    <div class="footer-copyright text-center py-3">© 2018 Copyright:
			    	<a href="index.php">MovieCafe.com</a>
			    </div>
		    </footer>    
		</body>
	</html>