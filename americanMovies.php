<?php  include_once ("templates/header.php")?>

			<div class="custom-modal" id="custom-modal-2">
			    <div class="row mt-5">
			        <div class="col-sm-10 mx-auto p-2 bg-light">
			            <form class="animate">
			                <div class="d-flex justify-content-between align-items-center">
			                    <span class="font-weight-bold">Rent Movie</span>
			                    <div>
			                        <button class="cancelTwo btn btn-danger btn-sm p-2 float-right" type="button">&times;</button>
			                    </div>
			                </div> 
			                <hr />
			                <div id="err-msgs" class="text-danger"></div>
			                <div>
			                    <input type="text" class="email form-control mb-2"  placeholder="Enter Email">
			                    <input type="text" class="title form-control mb-2"  placeholder="Enter Movie Title">
			                    <input type="text" class="address form-control mb-2" placeholder="Enter Home Address">
			                    <label for="Select">Select Quantity</label>
			                    <select class="custom-select">
			                      <option>1</option>
			                      <option>2</option>
			                      <option>3</option>
			                      <option>4</option>
			                      <option>5</option>
			                    </select>
			                </div>
			                <div class="text-right">
			                    <button class="btn btn-primary btn-sm btnn">Submit</button>
			                </div> 
			            </form> 
			        </div>
			    </div>
			</div>


				
			<div style="background:rgba(0,0,0,.5);  width:100%; height: 24vh; " class="d-flex justify-content-center flex-column text-white mt-5">
				<h1 class=" display-4 mx-auto">Top American  Movies</h1>
			</div>
			<div class="row mx-auto" >
					<div class="col-10 mx-auto">
						<h5 class="display-5 mt-2">Popular Films for you this week</h5>
						<div class="row justify-content-center pt-4">
							<div class="col-md-3 col-lg-3 mb-3">
								<div class="card">
	  								<img class="card-img-top img-fluid" src="images/JP.jpg" alt="Card image cap">
	 						 		<div class="card-body p-2">
	 						 			<h5 class="card-title">JurassicPark</h5>
	    								<p class="card-text">Director: Tayo <br />Duration: 2.40hrs<br />Genre‎: Drama, Science Fiction<br />Ratings: 9.5</p>
	    								<button class="btn btn-outline-secondary btn-block rent"> Rent</button>
	 						 		</div>
	 							</div>
							</div>

							<div class="col-md-3 col-lg-3 mb-3">
								<div class="card">
			  						<img class="card-img-top" src="images/915ju3JlUlL._RI_SX200_.jpg" alt="Card image cap">
			 						<div class="card-body p-2">
			 						 	<h5 class="card-title">Moris from America</h5>
			    						<p class="card-text">Director: Tayo <br/>Duration: 2.04hrs<br/>Genre‎: ‎Comedy<br/>Ratings: 8.4</p>
			    						<button class="btn btn-outline-secondary btn-block rent"> Rent</button>
		 						 	</div>
		 						</div>
							</div>

							<div class="col-md-3 col-lg-3 mb-3">
								<div class="card">
			  						<img class="card-img-top" src="images/download.jpg" alt="Card image cap">
			 						<div class="card-body p-2">
			 						 	<h5 class="card-title">Oceans' IIX</h5>
			    						<p class="card-text">Director: Tayo <br/>Duration: 1.45hrs<br/>Genre‎: ‎Action, Crime, Comedy<br/>Ratings: 7.5</p>
			    						<button class="btn btn-outline-secondary btn-block rent"> Rent</button>
	 						 		</div>
								</div>
							</div>

							<div class="col-md-3 col-lg-3 mb-3">
								<div class="card">
	  								<img class="card-img-top" src="images/the-room-bluray-ultraviolet-449921.7.jpg" alt="Card image cap">
	 						 		<div class="card-body p-2">
	 						 			<h5 class="card-title">The Room</h5>
	    								<p class="card-text">Director: Tayo <br/>Duration: 2.30hrs<br/>Genre: Drama, Story<br/>Ratings: 9.6</p>
	    								<button class="btn btn-outline-secondary btn-block rent"> Rent</button>
	 						 		</div>
								</div>
							</div>
							<div class="col-md-3 col-lg-3 mb-3">
								<div class="card">
	  								<img class="card-img-top" src="images/antman2.jpg" alt="Card image cap">
	 						 		<div class="card-body p-2">
	 						 			<h5 class="card-title">Ant-Man II</h5>
	    								<p class="card-text">Director: Tayo <br/>Duration: 2.05hrs<br/>Genre: Action, Comedy & Fantasy<br/>Ratings: 8.6</p>
	    								<button class="btn btn-outline-secondary btn-block rent"> Rent</button>
	 						 		</div>
								</div>
							</div>
							<div class="col-md-3 col-lg-3 mb-3">
								<div class="card">
	  								<img class="card-img-top" src="images/ThisLooksOkay.jpg" alt="Card image cap">
	 						 		<div class="card-body p-2">
	 						 			<h5 class="card-title">This looks okay</h5>
	    								<p class="card-text">Director: Tayo <br/>Duration: 2.30hrs<br/>Genre‎: ‎Comedy & Adventure, Drama<br/>Ratings: 9.6</p>
	    								<button class="btn btn-outline-secondary btn-block rent"> Rent</button>
	 						 		</div>
								</div>
							</div>
							<div class="col-md-3 col-lg-3 mb-3" >
								<div class="card">
			  						<img class="card-img-top" src="images/180302_front.jpg" alt="Card image cap">
			 						<div class="card-body p-2">
			 						 	<h5 class="card-title">Their Finest</h5>
			    						<p class="card-text">Director: Tayo <br/>Duration: 1.30hrs<br/>Genre‎: Drama, Romance, ‎Comedy<br/>Ratings: 8.0</p>
			    						<button class="btn btn-outline-secondary btn-block rent"> Rent</button>
		 						 	</div>
								</div>
							</div>
							<div class="col-md-3 col-lg-3 mb-3">
								<div class="card">
			  						<img class="card-img-top img-responsive" src="images/deadpoool2.png" alt="Card image cap">
			 						<div class="card-body p-2">
			 						 	<h5 class="card-title">	Deadpool II</h5>
			    						<p class="card-text">Director: Tayo <br/>Duration: 2.00hrs<br/>Genre‎:Action, Adenture, Sci-Fi, Comedy<br/>Ratings: 9.5</p>
			    						<button class="btn btn-outline-secondary btn-block rent"> Rent</button>
	 						 		</div>
								</div>
							</div>
							<p class="lead pb-3 mt-2 mx-auto">Note: Movies rented are to be returned on or before seven days after rental. </p>
						</div>
					</div>
					</div>
				</div>
			<?php  include_once ("templates/footer.php")?>