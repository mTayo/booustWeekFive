<?php require_once 'classes/dbclass.php';
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>crud api</title>
	 <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/style.css">
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/style2.css">
         <script type="text/javascript" src="bootstrap-4.0.0-beta/js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="bootstrap-4.0.0-beta/js/popper.min.js"></script>
        <script type="text/javascript" src="bootstrap-4.0.0-beta/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="bootstrap-4.0.0-beta/js/func.js"></script>

</head>
<body>
   <?php if (isset($_SESSION['user'])) { $mail=$_SESSION['user'];?>
   <div class="custom-modal" id="custom-modal-2">
    <div class="row mt-5">
        <div class="col-sm-10 mx-auto p-2 bg-light">
            <form class="animate insert" id="crudform" method="post" enctype="multipart/form-data" >
                <div class="d-flex justify-content-between align-items-center">
                    <span class="font-weight-bold">Upload new video</span>
                    <div>
                        <button class="cancelTwo btn btn-danger btn-sm p-2 float-right" type="button">&times;</button>
                    </div>
                </div> 
                <hr />
                
                    <input type="text" name="description" id="description" class="email form-control mb-2"  placeholder="Enter video description">
                    <input type="file" name="filename" id="video" class="title form-control mb-2" >
                    <input type="number"  name="rating" id="rating" class="address form-control mb-2" placeholder="rating">
                    <input type="hidden" name="mail" id="email" value="<?=$mail?>">
                    <input type="hidden" name="action" id="action" value="insert" />
                    
                <div class="text-danger" id="error-div"></div>
                <div class="text-success" id="success-div"></div>
                <div class="text-right">
                    <button class="btn btn-primary btn-sm btnn" id="post-button">Submit</button>
                </div> 
            </form> 
            </div>
        </div>
    </div>
</div>
<div class="container">

	
</div>

	<div class="table-responsive">
     <button class="btn btn-outline-secondary btn-block rent"style="width:50px;">add</button>
    <table class="table table-bordered table-striped">
     <thead>
      <tr>
       <th>Video url</th>
       <th>description</th>
        <th>Rating</th>
       <th>Edit</th>
       <th>Delete</th>
      </tr>
     </thead>
     <tbody></tbody>
    </table>
   </div>
</div>
	


</body>
<script type="text/javascript">
$(document).ready(function(){
 
  var email = <?= '"'. $_SESSION['user'].'"' ?>;
  
  function fetchVideo(){
    $.ajax({
       url:"/list.php?email="+email,
       success:function(data)
       {
        $('tbody').html(data);
       }
    })
  }

  fetchVideo();

})

  var files;
  $('input[type=file]').on('change',prepareUpload);
  function prepareUpload(event){
    files=event.target.files;
    //console.log(files);
  }

  $('#crudform').submit(function(e){
      e.preventDefault(); //console.log(document.querySelector('#video').files[0],files[0]);
        var form_description = $('#description').val(), 
            form_rating = $('#rating').val(), 
            form_mail   = $('#email').val(),
            form_action = $('#action').val(),
             post_data = new FormData();
             post_data.append('description',form_description);
             post_data.append('rating',form_rating);
             post_data.append('email',form_mail);
             post_data.append('action',form_action);
             post_data.append('description',form_description);
             post_data.append('video',document.querySelector('#video').files[0])
        var form = $('#crudform'),  success_div = $('#success-div'), error_div = $('#error-div'), post_button = $('#post-button');        
        $.ajax({
          url   :"/connection.php",
          method:"POST",
          data  :post_data,
          contentType: false,
          processData: false,
          beforeSend:function(){
              post_button.attr('disabled',true).text('Please Wait...')
          },
          success:function(response){
            // var result = JSON.parse(response); 
            console.log(response);
            // if(result.status == 'success'){
            //   form[0].reset();
            //   success_div.html(response.body);
            // }else{
            //   error_div.html(response.body);
            // }
            post_button.attr('disabled',false).html('submit');
          },
          error:function(){
            alert('An unknown error occured. please try again');
            post_button.attr('disabled',false).html('submit');
          }
        })



  })
  $(document).on('click', '.delete', function(){
  var id = $(this).attr("data-video-id");
  // console.log(id);
  var action = 'delete';
  var post_data;
   post_data = new FormData();
   post_data.append('id',id);
   post_data.append('action',action);
   $.ajax({
    url:"/connection.php",
    method:"POST",
    data  :post_data,
    contentType: false,
    processData: false,
    success:function(response)
    {
     
     alert("Data Deleted using PHP API");
      $('.text-success').html(response);
    }
   });
 
 });



</script>
   
</html>
 <?php }else{ ?>  

        <div class="display-4"> login first for api <a href="login.php" class="btn btn-info" role="button">Sign in</a></div>

        <?php 
        exit();
       }
       ?>
       