
<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8">
		  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<meta http-equiv="X-UA-Compatible" content="ie=edge">
			<title>
				 Register Movie Cafe Alpha
			</title>
			<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
			<link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/bootstrap.min.css">
			<link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/style.css">
			<style type="text/css">
				input{
					display: block;
					padding: 12px 20px;
					width: 75%;
					margin: 0 auto;
					margin-top: 3px;
				}
			</style>
		</head>
		<body>
			<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
				<a class="navbar-brand" href="index.php">Movie Cafe</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item">
							<a class="nav-link" href="index.php">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="login.php">Login</a>
						</li>
						 <li class="nav-item">   
                        <a class="nav-link" href="mail.php">Contact us</a>
                    </li>
                    <li class="nav-item">   
                        <a class="nav-link" href="newsLetter.php">Newsletter</a>
                    </li>
					</ul>
					<form class="form-inline my-2 my-lg-0">
					    <input class="form-control mr-sm-2" type="search" placeholder="Search for a Movie" aria-label="Search">
					    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search </button>
				    </form>
				</div>
			</nav>
			
			<div style="background:rgba(0,0,0,.5); height:20vh; width:100%;"class="d-flex justify-content-center flex-column text-white mt-5">
				<h1 class=" display-4 mx-auto">Register</h1>
			</div>
<?php

	session_start();
	require_once 'classes/dbclass.php';

	$username  = isset($_POST['username'] ) && !empty($_POST['username']) ? $_POST['username'] : '';
	$password = isset($_POST['password']) && !empty($_POST['password'])? $_POST['password'] : '';
	$compassword = isset($_POST['compassword']) && !empty($_POST['compassword'])? $_POST['compassword'] : '';
	$email = isset($_POST['email']) && !empty($_POST['email'])? $_POST['email'] : '';

	$username_error = $password_error = $compassword_error = $email_error= '';
	
	if($_SERVER['REQUEST_METHOD'] == 'POST'){

	
		$form_process= new login($username,$password,$email);
		// $form_process->createTable();
		// exit;
		//check fields	
		$has_errors = false;		
		if (!$form_process->validateName()) {
			$username_error ="Enter valid name";
			$has_errors = true;
		}
		if (!$form_process->validatePassword()) {
			$password_error ="Enter a valid password";
			$has_errors = true;
		}
		
		if ($password !== $compassword) {
			$compassword_error="Password and confirm password must match";
			$has_errors = true;
		}
		if(!$form_process->validateEmail()){
			$email_error ="Enter valid email address";
			$has_errors = true;
		}
		if(!$form_process->emailExists()){
			$email_error = 'Email already exists';
			$has_errors = true;
		}

		

	
?>

				<?php
			if (!$has_errors) {
			
			
			$form_process->insert();

			?>
			<div class="container">
				<p class="display-4 text-success">success <a href="login.php">Login to start session</a></p>
			</div>

		<?php }};?>
			<div id="display-errors" class="text-danger"></div>
	
			<form action="register.php" method="post" class="justify-content-center w-75 mx-auto mt-5">
				 <div class="form-group">
			<input type="text" name="username" value="<?= $username?>" placeholder="enter username">
			<span class="text-danger"><?= $username_error?></span>
		</div>
		 <div class="form-group">
			<input type="password" name="password" value="<?= $password?>" placeholder="enter password">
			<span class="text-danger"><?= $password_error?></span>
		</div>
		 <div class="form-group">
			<input type="password" name="compassword" value="<?= $compassword?>" placeholder="confirm password">
			<span class="text-danger"><?= $compassword_error?></span>
		</div>
		 <div class="form-group">
			<input type="text" name="email"value="<?= $email?>" placeholder="enter email">
			<span class="text-danger"><?= $email_error?></span>
		</div>
		<div class="form-group text-center">
			<button name="btn" class="btn btn-primary btn-lg">submit</button>
		</div>

		</form>
			<div class="container text-center">
				
				<p class="lead pb-1 mt-3 mx-auto">Register now for access to Home Delivery. </p>
				<p class="lead pb-3 mt-2 mx-auto">Note: Movies rented are to be returned on or before seven days after rental. </p>
			</div>
			

			<footer class="page-footer font-small cyan darken-3 text-center ">
		    	<div class="container">
			        <div class="row">
			        	<div class="col-md-12 py-5">
			          		<div class="mb-5 flex-center">
			            		<a class="fb-ic">
			              			<i class="fa fa-facebook fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
			            		</a>
			            		<a class="tw-ic">
			              			<i class="fa fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
			            		</a>
			            		<a class="gplus-ic">
			              			<i class="fa fa-google-plus fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
			            		</a>
					            <a class="li-ic">
					              	<i class="fa fa-linkedin fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
					            </a>
					            <a class="ins-ic">
					              	<i class="fa fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
					            </a>
					            <a class="pin-ic">
					              	<i class="fa fa-pinterest fa-lg white-text fa-2x"> </i>
					            </a>
			          		</div>
			       		</div>
			        </div>
			    </div>
			    <div class="footer-copyright text-center py-3">© 2018 Copyright:
			    	<a href="index.php">MovieCafe.herokuapp.com</a>
			    </div>
		    </footer>    
			
			
		</body>
		<script type="text/javascript" src="bootstrap-4.0.0-beta/js/jquery-3.2.1.min.js"></script>
			<script type="text/javascript" src="bootstrap-4.0.0-beta/js/popper.min.js"></script>
			<script type="text/javascript" src="bootstrap-4.0.0-beta/js/bootstrap.min.js"></script>
			<script type="text/javascript" src="bootstrap-4.0.0-beta/js/validate.js"></script>
		</html>