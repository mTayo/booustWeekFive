
<?php

	require_once 'classes/movieClasses.php';

	//$_SESSION['form_errors'] = false;
     //var_dump($_GET); exit();
	$name  = isset($_POST['name'] ) && !empty($_POST['name']) ? $_POST['name'] : '';
	$email = isset($_POST['email']) && !empty($_POST['email'])? $_POST['email'] : '';
	$essay = isset($_POST['essay']) && !empty($_POST['essay'])? $_POST['essay'] : '';

	$name_error = $email_error = $essay_error = '';
	//var_dump($_SERVER,$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].''.$_SERVER['REQUEST_URI'].'?status=success');exit();
	if($_SERVER['REQUEST_METHOD'] == 'POST'){

	
		$form_process= new formProcess();
		//check fields	
		$has_errors = false;		
		if (!$form_process->validateName($name)) {
			$name_error ="enter valid name";
			$has_errors = true;
		}
		if (!$form_process->validateMessage($essay)) {
			$essay_error ="Please leave a message";
			$has_errors = true;
		}
		if(!$form_process->validateEmail($email)){
			$email_error ="Enter valid email address";
			$has_errors = true;
		}

		if (!$has_errors) {
			
			$form_process->prop($name,$email,$essay);
			$form_process->saveincsv();
			$form_process->sendMail();
			
		}

	}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/style.css">
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/style2.css">
        <title>Contact us</title>
        <style type="text/css">
	
	span{
		color: red;
		margin: 5px 0;
	}
	.container{
		margin-top: 20vh;
	}
</style>
    </head>
    <body>
    	 <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
            <a class="navbar-brand" href="index.php">Movie Cafe</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Home</a>
                    </li>
                    <li class="nav-item dropdown" >
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" data-toggle="dropdown">Lastest movies</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="nigerianMovies.html">Nigerian Movies</a>
                            <a class="dropdown-item" href="americanMovies.html">American movies/Series</a>
                            <a class="dropdown-item" href="asian.html">Asian Movies</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="seelocations.html">Locations</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="register.html">Register</a>
                    </li>
                    <li class="nav-item">   
                        <a class="nav-link" href="mail.php">Contact us</a>
                    </li>
                    <li class="nav-item">   
                        <a class="nav-link" href="newsLetter.php">Newsletter</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="services.html">Services</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                      <input class="form-control mr-sm-2" type="search" placeholder="Search for a Movie" aria-label="Search">
                      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search </button>
                </form>
            </div>
        </nav>



	
	<?php 
       
		if (isset($_GET['status']) && ($_GET['status'] == 'success')) {
	?>
		<div class="container" style="margin-top: 20vh">

			<h1 class="display-4 text-center" >Thank you</h1>

		</div>

	<?php } else {?>


		<div class="container text-center">

            <?php if (isset($_GET['status']) && ($_GET['status'] == 'error')) {  ?>

                <h4 class=" text-center text-danger" >Could not process request. please try again</h4>

            <?php } ?>
			<p class="display-4">Contact us</p>
			<form action="mail.php" method="post">
    			<div class="form-group">
    				<input type="text" name="name" class="mb-2 form-control" placeholder="Enter name" value="<?= $name?>">
    				<span><?= $name_error?></span>
                </div>
                <div class="form-group">
    				<input type="text" name="email" class="mb-2 form-control"  placeholder="Enter Email" value="<?= $email?>">
    				<span><?= $email_error?></span>
                </div>
                 <div class="form-group">
    				<textarea  name="essay" class="mb-2 form-control" value="<?= $essay?>"placeholder="Enter Message/Essay"></textarea>
    				<span><?= $essay_error?></span>
                </div>
    				<button class="btn btn-primary btn-sm mt-3" name="submit">Submit</button>
    			</div>
			</form>
		</div>
		<form>
>



	<?php } ?>
	  <footer class="page-footer font-small cyan darken-3 text-center w-100">
            <div class="row mx-auto">
                <div class="col-md-12 py-5">
                  <div class="mb-5 flex-center">
                        <a class="fb-ic">
                          <i class="fa fa-facebook fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                        </a>
                        <a class="tw-ic">
                          <i class="fa fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                        </a>
                        <a class="gplus-ic">
                          <i class="fa fa-google-plus fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                        </a>
                        <a class="li-ic">
                          <i class="fa fa-linkedin fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                        </a>
                        <a class="ins-ic">
                          <i class="fa fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                        </a>
                        <a class="pin-ic">
                          <i class="fa fa-pinterest fa-lg white-text fa-2x"> </i>
                        </a>
                  </div>
                </div>
            </div>
            <div class="footer-copyright text-center py-3">© 2018 Copyright:
              <a href="index.php">MovieCafe.herokuapp.com</a>
            </div>
          </footer>

</body>
</html>