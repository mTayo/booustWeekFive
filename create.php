<?php
require_once 'classes/dbclass.php';
session_start();
include("connection.php");
if (is_null($_SESSION['user'])) {
		die("login first to use api");
	}
	$db = new dbObj();
	$connection =  $db->dbConnect();
	//var_dump($connection);exit;
	$request_method=$_GET['go'];
	
	$mail=$_SESSION['user'];
	
	switch($request_method)
	{
		case 'insert':
		 insert_video();
		 break;

		case 'update':
		//$id=intval($_GET["id"]);
		update_video($mail);
		break;
		
		 case 'delete':
		//$id=intval($_GET["id"]);
		delete_video($mail);
		 break;
		case 'view':
		// //$id=intval($_GET["id"]);
		get_video($mail);
		 break;
		
		
		default:
		// Invalid Request Method
		header("HTTP/1.0 405 Method Not Allowed");
		break;

	}
	function update_video($mail)
	{
		global $connection;
		$mail=$_SESSION['user'];
		$path= 'https://cinemacafe.s3.amazonaws.com/';
		$location=$path.$_GET['file'];
		$sql="UPDATE `video` SET `description`=:description, `location`=:location WHERE `email`=:email";
		$stmt=$connection->prepare($sql);

		//var_dump($stmt);exit();
			$stmt->bindParam(":description", $_GET['description']); 
			$stmt->bindParam(":location",$location);  
			$stmt->bindParam(':email', $mail); 
		if($stmt->execute())
		{
			$response=array(
				'status' => 1,
				 "description"=>$_GET['description'],
				 "location"=>$location,
				  "author"=>$mail,
				'status_message' =>' Updated Successfully.'
			);
		}else
		{
			$response=array(
				'status' => 0,
				'status_message' =>' Updation Failed.'
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);

	}

function delete_video($mail)
{
	global $connection;
	$query="DELETE FROM video WHERE email=?";
	$statement=$connection->prepare($query);
	$statement->bindParam(':email',$mail);
	if($statement->execute([$mail])){
		$response=array(
			'status' => 1,
			'status_message' =>'video Deleted Successfully.'
		);
		}else
	{
		$response=array(
			'status' => 0,
			'status_message' =>'video Deletion Failed.'
		);
	}
	header('Content-Type: application/json');
	echo json_encode($response);
}

	function get_video($mail)
	{
		
		$mail=$_SESSION['user'];
		global $connection;
		$sql="SELECT * FROM video WHERE email=? ORDER BY ID DESC LIMIT 4";
		$stmt=$connection->prepare($sql);
		
		$stmt->bindParam(':email', $mail);
		$stmt->execute([$mail]);
		$response=array();
		while ( $result = $stmt->fetch() ) {
		{
			$response[]=$result;
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}
}
	function insert_video()
	{
		global $connection;
		$mail=$_SESSION['user'];
		$path= 'https://cinemacafe.s3.amazonaws.com/';
		$location=$path.$_GET['file'];
	$sql = "INSERT INTO video (description,location,email) VALUES(:description,:location,:email)";
	$stmt=$connection->prepare($sql);
	$stmt->bindParam(':description',$_GET['description']);
            $stmt->bindParam(':location',$location);
            $stmt->bindParam(':email',$mail);	
             if ($stmt->execute()) {

			$response=array(
				'status' => 1,
				'location'=>$location,
				'description'=>$_GET['description'],
				'author'=>$mail,
				'status_message' =>'video Added Successfully.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'video Addition Failed.'
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}
?>
<?php
require_once 'classes/dbclass.php';
session_start();
require('vendor/autoload.php');
 $s3 = Aws\S3\S3Client::factory();
 $bucket = getenv('S3_BUCKET')?: 'cinemacafe';
if (is_null($_SESSION['user'])) {
    die("login first to use api");
  }


if(isset($_POST['Submit'])){
  $fileName = $_FILES['file']['name'];
  $fileTempName = $_FILES['file']['tmp_name'];

  //var_dump($fileName);exit;
  $position= strpos($fileName, "."); 
  $fileextension= substr($fileName, $position + 1);
  $fileextension= strtolower($fileextension);

  $description= $_POST['description_entered'];
  $success= -1;
  $descript= 0;
  if (empty($description))
  {
  $descript= 1;

  }

    if (isset($fileName)) {
      $path= 'https://cinemacafe.s3.amazonaws.com/';
      if (!empty($fileName)){
        if (($fileextension !== "mp4") && ($fileextension !== "ogg") && ($fileextension !== "webm")){
          $success=0;
          echo "The file extension must be .mp4, .ogg, or .webm in order to be uploaded";

        }
        else if (($fileextension == "mp4") || ($fileextension == "ogg") || ($fileextension == "webm")){
        $success=1;
        $location=$path.$fileName;

         try {
            $upload = $s3->upload($bucket, $fileName, fopen($fileTempName, 'rb'), 'public-read');
      //       $video_process=new userDash("","","","","","",$email,$description,$fileName,$location,"");
        // $video_process->insertvideo();
            ?> <p>Upload <a href="<?=htmlspecialchars($upload->get('ObjectURL'))?>">successful</a> :)</p>
        <?php } catch(Exception $e) { ?>
              <p>Upload error :(</p>
        <?php }
      }

    }
    }
    }
    ?>


?>