<?php

    session_start();
    require_once 'classes/dbclass.php';

?>



<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/style.css">
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/style2.css">
        <title>Welcome to Movie Cafe</title>
    </head>
    <body>

<!-- all custom modals -->
<!-- sign in -->
<div class="custom-modal text-dark" id="custom-modal-1">
    <div class="row mt-5">
        <div class="col-sm-10 mx-auto p-2 bg-light">
            <form class="form animate">
                <div class="d-flex justify-content-between align-items-center">
                    <span class="font-weight-bold text-dark">Sign in</span>
                    <div>
                        <button class="cancel-sign-in btn btn-danger btn-sm p-2 float-right" type="button">&times;</button>
                    </div>
                </div> 
                <hr />
                <div id="display-errors" class="text-danger text-left"></div>
                <input type="text" name="email" id="email" class="email mb-3 form-control" placeholder="Email">
                <input type="password" name="pass" id="pass" class="pass mb-3 form-control" placeholder="Enter Password">
                <div class="text-center">
                    <button class="btn btn-primary" id="sign-in-submit" type="submit">Submit</button>
                </div>
                <p>Note : Movies to be returned 7 days after rental.</p>
        
            </form>
        
        </div>
    </div>
</div>
<!--./end signin  -->
<!-- rental -->
<div class="custom-modal" id="custom-modal-2">
    <div class="row mt-5">
        <div class="col-sm-10 mx-auto p-2 bg-light">
            <form class="animate">
                <div class="d-flex justify-content-between align-items-center">
                    <span class="font-weight-bold">Rent Movie</span>
                    <div>
                        <button class="cancelTwo btn btn-danger btn-sm p-2 float-right" type="button">&times;</button>
                    </div>
                </div> 
                <hr />
                <div id="err-msgs" class="text-danger"></div>
                <div>
                    <input type="text" class="email form-control mb-2"  placeholder="Enter Email">
                    <input type="text" class="title form-control mb-2"  placeholder="Enter Movie Title">
                    <input type="text" class="address form-control mb-2" placeholder="Enter Home Address">
                    <label for="Select">Select Quantity</label>
                    <select class="custom-select">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                </div>
                <div class="text-right">
                    <button class="btn btn-primary btn-sm btnn">Submit</button>
                </div> 
            </form> 
        </div>
    </div>
</div>
<!--/.end rental  -->

        <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
            <a class="navbar-brand" href="index.php">Movie Cafe</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Home</a>
                    </li>
                    <li class="nav-item dropdown" >
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" data-toggle="dropdown">Lastest movies</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="nigerianMovies.php">Nigerian Movies</a>
                            <a class="dropdown-item" href="americanMovies.php">American movies/Series</a>
                            <a class="dropdown-item" href="asian.php">Asian Movies</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="seelocations.php">Locations</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="register.php">Register</a>
                    </li>
                    <li class="nav-item">   
                        <a class="nav-link" href="newsletter.php">Newsletter</a>
                    </li>
                    <li class="nav-item">   
                        <a class="nav-link" href="mail.php">ContactUs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="services.php">Services</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                      <input class="form-control mr-sm-2" type="search" placeholder="Search for a Movie" aria-label="Search">
                      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search </button>
                </form>
            </div>
        </nav>
        <div class="banner">
            <div style="background:rgba(0,0,0,.5); height:100%; width:100%; " class="d-flex justify-content-center flex-column text-white">
                <h1 class=" display-4">  Welcome to Movie Cafe Alpha</h1>
                <p class="lead">Home to High Definition Movies<br/>Experience High Quality like never before</p>    
                <div class="sign">
     <?php if (isset($_SESSION['user'])) {?>
        <div class="container">
            welcome <?php echo ($_SESSION['user']);?>
            <form action="index.php" method="post">
            <button name="logout" class="btn btn-white">logout</button>
            </form> 
            
        </div>
         <?php if (isset($_POST['logout'])) {
         $logout=new login();
         $logout->logout();
         }?>   
            

             <?php }else{ ?>    
         <a href="login.php" class="btn btn-info" role="button">Sign in</a>
        <?php }?>
   
                </div>
            </div>
        </div>
            <div class="container-fluid ">
                <div class="container text-center ">
                    <h2 >We are glad to announce that we Offically opened 6 new branches all over Lagos State today</h2>
                    <p class="lead"> We bring the Alpha experience to your doorstep. Rent cool movies at any location closest to you with ease for a minimal price.<br/>Enjoy high quality, Enjoy with Movie Cafe.</p>
                    <a class="btn btn-primary btn-lg btn-md mt-2 mb-3 " href="seelocations.php" role="button">See Locations</a>
                </div>      
                <div class="row" >
                    <div class="col-10 mx-auto" >
                        <h5 class="display-5 mt-2">Popular Films for you this week</h5>
                        <div class="rentz">
                            <div class="row justify-content-center pt-4">
                    
                                <div class="col-md-3 col-lg-3 mb-3">
                                    <div class="card">
                                        <img class="card-img-top img-fluid" src="images/JP.jpg" alt="Card image cap">
                                        <div class="card-body p-2">
                                            <h5 class="card-title">JurassicPark</h5>
                                            <p class="card-text">Director: Tayo <br/>Duration: 2.40hrs<br/>Genre: Drama, Science Fiction<br />Ratings: 9.5</p>
                                            <button class="btn btn-outline-secondary btn-block rent"> Rent</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-lg-3 mb-3">
                                    <div class="card">
                                        <img class="card-img-top" src="images/Aquaman_DCEU.jpg" alt="Card image cap">
                                        <div class="card-body p-2">
                                            <h5 class="card-title">Aquaman</h5>
                                            <p class="card-text">Director: Tayo <br/>Duration: 2.30hrs<br/>Genre: Action, Science Fiction.<br/>Ratings: 8.4</p>
                                           <button class="btn btn-outline-secondary btn-block rent"> Rent</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-lg-3 mb-3">
                                    <div class="card">
                                        <img class="card-img-top" src="images/download.jpg" alt="Card image cap">
                                        <div class="card-body p-2">
                                            <h5 class="card-title">Oceans' IIX</h5>
                                             <p class="card-text">Director: Tayo <br/>Duration: 2.30hrs<br/>Genre: Action, Comedy, Fantasy<br/>Ratings: 9.6</p>
                                           <button class="btn btn-outline-secondary btn-block rent"> Rent</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-lg-3 mb-3">
                                    <div class="card">
                                        <img class="card-img-top" src="images/A.I.W.jpg" alt="Card image cap">
                                        <div class="card-body p-2">
                                            <h5 class="card-title">Infinity war</h5>
                                            <p class="card-text">Director: Tayo <br/>Duration: 2.30hrs<br/>Genre: Action, Comedy, Fantasy<br/>Ratings: 9.6</p>
                                            <button class="btn btn-outline-secondary btn-block rent"> Rent</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 mb-3">
                                    <div class="card">
                                        <img class="card-img-top" src="images/antman2.jpg" alt="Card image cap">
                                        <div class="card-body p-2">
                                            <h5 class="card-title">Ant-Man II</h5>
                                            <p class="card-text">Director: Tayo <br/>Duration: 2.05hrs<br/>Genre: Action, Comedy & Fantasy<br/>Ratings: 8.6</p>
                                            <button class="btn btn-outline-secondary btn-block rent"> Rent</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 mb-3">
                                    <div class="card">
                                        <img class="card-img-top" src="images/ThisLooksOkay.jpg" alt="Card image cap">
                                        <div class="card-body p-2">
                                            <h5 class="card-title">This looks okay</h5>
                                            <p class="card-text">Director: Tayo <br/>Duration: 2.05hrs<br/>Genre: Adventure, Comedy & Drama<br/>Ratings: 9.6</p>
                                           <button class="btn btn-outline-secondary btn-block rent"> Rent</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 mb-3" >
                                    <div class="card">
                                        <img class="card-img-top" src="images/incredible.jpg" alt="Card image cap">
                                        <div class="card-body p-2">
                                            <h5 class="card-title">IncrediblesII</h5>
                                            <p class="card-text">Director: Tayo <br/>Duration: 2.05hrs<br/>Genre: Action, Kids, Comedy & Family<br/>Ratings: 8.0</p>
                                           <button class="btn btn-outline-secondary btn-block rent"> Rent</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 mb-3">
                                    <div class="card">
                                        <img class="card-img-top img-responsive" src="images/deadpoool2.png" alt="Card image cap">
                                        <div class="card-body p-2">
                                            <h5 class="card-title"> Deadpool II</h5>
                                            <p class="card-text">Director: Tayo <br/>Duration: 2.05hrs<br/>Genre: Action, Adventure , Sci-Fi & Comedy<br/>Ratings: 9.5</p>
                                            <button class="btn btn-outline-secondary btn-block rent"> Rent</button>
                                        </div>
                                    </div>
                                </div>
                                <p class="lead pb-3 mt-2 mx-auto">Note: Movies rented are to be returned on or before seven days after rental. </p>
                            </div>
                            <div class="text-md-right mt-0">
                                <a class="btn btn-primary btn-sm" href="index.php" role="button">Click for more</a>
                            </div>
                        </div>
                    </div>
                <div class="container d-flex justify-content-center bg-light">
                    <div class="w-75  p-3">
                        <h2 class="display-5 text-center ">Coming soon to Movie Cafe</h2>
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                      <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                      </ol>
                      <div class="carousel-inner">
                        <div class="carousel-item active">
                          <img class="d-block w-100" src="images/Dwayne-Johnson-Skyscraper-2018.jpg" alt="Skyscraper.jpg">
                          <div class="carousel-caption d-none d-md-block">
                        <h5>Skyscraper</h5>
                        <p>Genre: Action, Date: 20th July, 2018.<br/><a href="https://www.youtube.com/watch?v=t9QePUT-Yt8"> Click to see Trailer</a></p>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <img class="d-block w-100" src="images/maxresdefault.jpg" alt="Mission Impossible.jpg">
                      <div class="carousel-caption d-none d-md-block">
                        <p style="color: black">Genre: Action & Adventure, Date:25th July, 2018<br/><a href="https://www.youtube.com/watch?v=XiHiW4N7-bo"> Click to see Trailer</a></p>
                      </div>
                    </div>

                    <div class="carousel-item">
                      <img class="d-block w-100" src="images/smallfoot2.jpg" alt="smallfoot.jpg">
                      <div class="carousel-caption d-none d-md-block">
                    <h5>Small foot</h5>
                    <p>Genre: Animation, kids. Date:25th July, 2018.<br/><a href="https://www.youtube.com/watch?v=rGFJHElc5VU"> Click to see Trailer</a> </p>
                  </div>
                    </div>
                     <div class="carousel-item">
                      <img class="d-block w-100" src="images/mamma-mia-here-we-go-again-film-1300x731.jpg" alt="Mamma-mia.jpg">
                      <div class="carousel-caption d-none d-md-block">
                    <h5>Mamma-mia here we go again</h5>
                    <p>Genre: Comedy & Family, Date:28th July, 2018<br/><a href="https://www.youtube.com/watch?v=nd4cLL_MP7M"> Click to see Trailer</a></p>
                  </div>
                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
            </div>
        </div>
        </div>
        <footer class="page-footer font-small cyan darken-3 text-center w-100">
            <div class="row mx-auto">
                <div class="col-md-12 py-5">
                  <div class="mb-5 flex-center">
                        <a class="fb-ic">
                          <i class="fa fa-facebook fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                        </a>
                        <a class="tw-ic">
                          <i class="fa fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                        </a>
                        <a class="gplus-ic">
                          <i class="fa fa-google-plus fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                        </a>
                        <a class="li-ic">
                          <i class="fa fa-linkedin fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                        </a>
                        <a class="ins-ic">
                          <i class="fa fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                        </a>
                        <a class="pin-ic">
                          <i class="fa fa-pinterest fa-lg white-text fa-2x"> </i>
                        </a>
                  </div>
                </div>
            </div>
            <div class="footer-copyright text-center py-3">© 2018 Copyright:
              <a href="index.php">MovieCafe.herokuapp.com</a>
            </div>
          </footer>
        <script type="text/javascript" src="bootstrap-4.0.0-beta/js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="bootstrap-4.0.0-beta/js/popper.min.js"></script>
        <script type="text/javascript" src="bootstrap-4.0.0-beta/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="bootstrap-4.0.0-beta/js/func.js"></script>
        <script type="text/javascript" src="bootstrap-4.0.0-beta/js/validate.js"></script>

</body>
</html>