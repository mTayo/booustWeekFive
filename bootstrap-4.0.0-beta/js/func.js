$(function(){

	$(".bttn").click(function(){
		$(".custom-modal").fadeToggle(1500).css("display","block")
	});

	$('#sign-in').click(function(){
		$("#custom-modal-1").fadeToggle(1500).css("display","block")
	});
	$('.cancel-sign-in').click(function(){
		$("#custom-modal-1").css("display","none")
	});

	$(".cancel").click(function(){
		$(".custom-modal").css("display","none")
	});

	$(".rent").click(function(){
		$("#custom-modal-2").fadeToggle(1500).css("display","block")
	});

	$(".cancelTwo").click(function(){
		$("#custom-modal-2").css("display","none")
	});
});
