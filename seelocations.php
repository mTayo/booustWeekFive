<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8">
		  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<meta http-equiv="X-UA-Compatible" content="ie=edge">
			<title>
			Locations Movie Cafe Alpha
			</title>
			<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
			<link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/bootstrap.min.css">
			<link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/style.css">
			<script type="text/javascript" src="bootstrap-4.0.0-beta/js/jquery-3.2.1.min.js"></script>
			<script type="text/javascript" src="bootstrap-4.0.0-beta/js/popper.min.js"></script>
			<script type="text/javascript" src="bootstrap-4.0.0-beta/js/bootstrap.min.js"></script>
		</head><body>
			<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
				<a class="navbar-brand" href="index.php">Movie Cafe</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item">
							<a class="nav-link" href="index.php">Home</a>
						</li>
						<li class="nav-item dropdown" >
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" data-toggle="dropdown">Lastest movies</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="nigerianMovies.php">Nigerian Movies</a>
								<a class="dropdown-item" href="americanMovies.php">American movies/Series</a>
								<a class="dropdown-item" href="asian.php">Asian Movies</a>
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="boxoffice.php">Box Office</a>
						</li>
						 <li class="nav-item">   
                        <a class="nav-link" href="mail.php">Contact us</a>
                    </li>
                    <li class="nav-item">   
                        <a class="nav-link" href="newsLetter.php">Newsletter</a>
                    </li>
						<li class="nav-item">
							<a class="nav-link" href="services.php">Services</a>
						</li>
					</ul>
					 <form class="form-inline my-2 my-lg-0">
					      <input class="form-control mr-sm-2" type="search" placeholder="Search for a Movie" aria-label="Search">
					      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search </button>
				    </form>
				</div>
			</nav>
				
				<div style="background:rgba(0,0,0,.5);  width:100%; height:20vh; " class="d-flex justify-content-center flex-column text-white mt-5">
					<h1 class=" display-4 mx-auto">See Locations</h1>
				</div>
			
			  <div class="container w-100 mt-4 " >
				    <div class="media  mb-3 mediaI">
				  		<img class="align-self-center mr-3" src="images/diurno.jpg" height="250px" width="300px" alt="Generic placeholder image">
				  		<div class="media-body mt-2">
				    		<h5 class="mt-1">Yaba office</h5>
				    		<p class="lead">Address: 1234, ilemoboye street, Off Custodian Office</p>
				  		</div>
					</div>
				 	<div class="media mb-3 mediaI">
				  		<img class="align-self-center mr-3" src="images/elcorteingles3.jpg" height="250px" width="300px" alt="Generic placeholder image">
				  		<div class="media-body mt-2">
				   			<h5 class="mt-1">Okota Office</h5>
				    		<p class="lead">Address: 1632, Sijuola Street,  Ago Palace way.</p>
				  		</div>
					</div>
				 <div class="media mb-3 mediaI">
				  <img class="align-self-center mr-3" src="images/1406625890169_464882Spain2_464882.jpg" height="250px" width="300px" alt="Generic placeholder image">
				  <div class="media-body mt-2">
				    <h5 class="mt-0">Victoria island</h5>
				    <p class="lead">Address: 3230, ilemoboye street, Opposite Eko Hotel and suites</p>
				  </div>
				</div>
				 <div class="media mb-3 mediaI">
				  <img class="align-self-center mr-3" src="images/images (1).jpg" height="250px" width="300px" alt="Generic placeholder image">
				  <div class="media-body mt-2">
				    <h5 class="mt-0">Ikoyi Office</h5>
				    <p class="lead">Address: 34, Opemide street, Opposite 2ilabs Ikoyi</p>
				  </div>
				</div>
				<div class="media mb-3 mediaI">
				  <img class="align-self-center mr-3" src="images/download (7).jpg" height="250px" width="300px" alt="Generic placeholder image">
				  <div class="media-body mt-2">
				    <h5 class="mt-0">Bannana Island Office</h5>
				    <p class="lead">Address: 1, Bannana street, Off Igbo Ogede</p>
				  </div>
				</div>
				<div class="media mb-3 mediaI">
				  <img class="align-self-center mr-3" src="images/d-modern-office-building-architecture-exterior-design-white-background-create-38552162.jpg" height="250px" width="300px" alt="Generic placeholder image">
				  <div class="media-body mt-2">
				    <h5>Surulere office</h5>
				    <p class="lead">Address: 1234, Bode Thomas, Close to MTN office</p>
				  </div>
				</div>
			</div>
			<div class="text-center"><p class="lead">Locate any office close to you and do grab a movie now!</p></div>
			<footer class="page-footer font-small cyan darken-3 text-center ">
		    	<div class="container">
			        <div class="row ">
			        	<div class="col-md-12 py-5">
			          		<div class="mb-5 flex-center">
			            		<a class="fb-ic">
			              			<i class="fa fa-facebook fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
			            		</a>
			            		<a class="tw-ic">
			              			<i class="fa fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
			            		</a>
			            		<a class="gplus-ic">
			              			<i class="fa fa-google-plus fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
			            		</a>
					            <a class="li-ic">
					              	<i class="fa fa-linkedin fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
					            </a>
					            <a class="ins-ic">
					              	<i class="fa fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
					            </a>
					            <a class="pin-ic">
					              	<i class="fa fa-pinterest fa-lg white-text fa-2x"> </i>
					            </a>
			          		</div>
			       		</div>
			        </div>
			    </div>
			    <div class="footer-copyright text-center py-3">© 2018 Copyright:
			    	<a href="index.php">MovieCafe.herokuapp.com</a>
			    </div>
		    </footer>    
		</body>
		</html>