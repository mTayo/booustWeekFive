  
  <?php

    session_start();
    require_once 'classes/dbclass.php';

?>



<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/style.css">
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/style2.css">
        <title>Welcome to Movie Cafe</title>
    </head>
    <body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
            <a class="navbar-brand" href="index.php">Movie Cafe</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Home</a>
                    </li>
                    <li class="nav-item dropdown" >
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" data-toggle="dropdown">Lastest movies</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="nigerianMovies.php">Nigerian Movies</a>
                            <a class="dropdown-item" href="americanMovies.php">American movies/Series</a>
                            <a class="dropdown-item" href="asian.php">Asian Movies</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="seelocations.php">Locations</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="register.php">Register</a>
                    </li>
                    <li class="nav-item">   
                        <a class="nav-link" href="newsletter.php">Newsletter</a>
                    </li>
                    <li class="nav-item">   
                        <a class="nav-link" href="mail.php">ContactUs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="services.php">Services</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                      <input class="form-control mr-sm-2" type="search" placeholder="Search for a Movie" aria-label="Search">
                      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search </button>
                </form>
            </div>
        </nav>