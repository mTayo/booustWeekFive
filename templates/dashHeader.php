
<?php
require_once 'classes/dbclass.php';
session_start();
require_once 'classes/userclass.php';
?>
<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8">
		  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<meta http-equiv="X-UA-Compatible" content="ie=edge">
			<title>
				 User Dashboard
			</title>
			<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
			<link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/bootstrap.min.css">
			<link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/dash.css">
		</head>
		<body>
			 <?php if (isset($_SESSION['user'])) {?>
			<div class="header">
				
				<div class="head navbar-brand color"> <?php echo ($_SESSION['username'])."'s Dashboard Space ";?> </div>
			</div>
			<div class="main">
			<div class="sidebar">
				<img class="img-responsive rounded-circle" src="images/images.jpeg">
				
				<ul id="nav">
					<li><a href="createprofile.php">Create profile</a></li>
					<li><a href="update.php">Update profile</a></li>
					<li><a href="view.php">View profile</a></li>
					<li><a href="createVideo.php">Create video</a></li>
			
					
				</ul>
				<form action="index.php" method="post">
				<button name="logout" class="btn btn-white">logout</button>	
				</form>
			</div>
		
			
			 <?php if (isset($_POST['logout'])) {
		         $logout=new login();
		         $logout->logout();
		         }?>   
            
		
			 <?php }else{ ?>  

			 	<div class="display-4"> Nothing to display! login first <a href="login.php" class="btn btn-info" role="button">Sign in</a></div>

			 	<?php 
			 	exit();
			 }
			 ?>