
	<?php require 'calLeapYear.php'; ?>
<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
		  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<meta http-equiv="X-UA-Compatible" content="ie=edge">
			<meta name="viewport" content="width=device-width,initial-scale=1.0">
			<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
			<link rel="stylesheet" type="text/css" href="bootstrap-4.0.0-beta/css/style3.css">
			<title>Check for Leap Year</title>	
		</head>
		<body>
			<div class="overlay"> </div>
			<div id="container">
				<div class="nav">
				<h2 class="header"><a href="index.php">Movie-cafe</a></h2>
				<h1></h1>
				</div>
				<div class="nav-2">
					<p>Leap-year Calculator</p>
				</div>
				<div class="main">
					<div class="display-1"><p class="display-1-p" style="padding-top: 12%;">Welcome to Movie-Cafes' quick leap year calculator</p></div>
					<div class="display-2"><p class="display-3-p mx-auto">Scroll down to see the list of leap years we have prepared for you from the year 1980 to 2018.</p></div>
					<div class="main-2">
							<div class="sub-main1"><h2 class="display-2-p bb">List of leap years from 1980 - 2018</h2><p class="display-2-p mx"><?php leap() ?></p></div>
							<div class="sub-main1"> 
								<h2 class="display-2-p bb">Enter a year to check if its a leap year </h2>
								<p class="display-2-p mx">This section checks if the input (Enter Year) is a leap year. If it is, it returns the value entered as a leap year but if it isn't, it runs a loop to check for the next leap year.</p>
								<form  action=" " method="post" class="mx">
									<input type="number" name="year" id="year"  placeholder="Enter Year"/>
									<button>Submit<br/></button><br/>
									<?php
									if (isset ( $_POST["year"])) {
										$value= $_POST['year'];
											verifyLeap($value);
										};
									?>
								</form>
							</div>
					</div>
				</div>
			</div>
		
				<div class="footer">
					<p>you can navigate to our main site @ <a href="index.php">Movie-cafe.herokuapp.com</a></p>
					<p>MovieCafe &copy copyright,  2018</p>
				</div>
			</div>
		</body>
	</html>
