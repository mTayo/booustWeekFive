<?php  include_once ("templates/header.php")?>
			<div class="custom-modal" id="custom-modal-2">
			    <div class="row mt-5">
			        <div class="col-sm-10 mx-auto p-2 bg-light">
			            <form class="animate">
			                <div class="d-flex justify-content-between align-items-center">
			                    <span class="font-weight-bold">Rent Movie</span>
			                    <div>
			                        <button class="cancelTwo btn btn-danger btn-sm p-2 float-right" type="button">&times;</button>
			                    </div>
			                </div> 
			                <hr />
			                <div id="err-msgs" class="text-danger"></div>
			                <div>
			                    <input type="text" class="email form-control mb-2"  placeholder="Enter Email">
			                    <input type="text" class="title form-control mb-2"  placeholder="Enter Movie Title">
			                    <input type="text" class="address form-control mb-2" placeholder="Enter Home Address">
			                    <label for="Select">Select Quantity</label>
			                    <select class="custom-select">
			                      <option>1</option>
			                      <option>2</option>
			                      <option>3</option>
			                      <option>4</option>
			                      <option>5</option>
			                    </select>
			                </div>
			                <div class="text-right">
			                    <button class="btn btn-primary btn-sm btnn">Submit</button>
			                </div> 
			            </form> 
			        </div>
			    </div>
			</div>




			<div style="background:rgba(0,0,0,.5); height: 24vh; width:100%; " class="d-flex justify-content-center flex-column text-white mt-5">
				<h1 class=" display-4 mx-auto">Top Nigerian Movies</h1>
			</div>
			 

			<div class="row mx-auto"  >
					<div class="col-10 mx-auto">
						
						<div class="row justify-content-center pt-4">
							<div class="col-md-3 col-lg-3 mb-3">
								<div class="card">
	  								<img class="card-img-top img-fluid" src="images/download (3).jpg" alt="Card image cap">
	 						 		<div class="card-body p-2">
	 						 			<h5 class="card-title">Teckno in villa</h5>
	    								<p class="card-text">Director: Tayo <br />Duration: 1.30hrs<br />Genre‎: Drama, Comedy, Village<br />Ratings: 5.5</p>
	    								<button class="btn btn-outline-secondary btn-block rent"> Rent</button>
	 						 		</div>
	 							</div>
							</div>

							<div class="col-md-3 col-lg-3 mb-3">
								<div class="card">
			  						<img class="card-img-top" src="images/download (1).jpg" alt="Card image cap">
			 						<div class="card-body p-2">
			 						 	<h5 class="card-title">Womans' cot</h5>
			    						<p class="card-text">Director: Tayo <br/>Duration: 2.00hrs<br/>Genre‎: ‎Drama, Family.<br/>Ratings: 8.1</p>
			    						<button class="btn btn-outline-secondary btn-block rent"> Rent</button>
		 						 	</div>
		 						</div>
							</div>

							<div class="col-md-3 col-lg-3 mb-3">
								<div class="card">
			  						<img class="card-img-top" src="images/ruthless businessman.jpg" alt="Card image cap">
			 						<div class="card-body p-2">
			 						 	<h5 class="card-title">Ruthless Businessman</h5>
			    						<p class="card-text">Director: Tayo <br/>Duration: 1.45hrs<br/>Genre‎: ‎Action, Crime, Comedy<br/>Ratings: 7.9</p>
			    						<button class="btn btn-outline-secondary btn-block rent"> Rent</button>
	 						 		</div>
								</div>
							</div>

							<div class="col-md-3 col-lg-3 mb-3">
								<div class="card">
	  								<img class="card-img-top" src="images/download (2).jpg" alt="Card image cap">
	 						 		<div class="card-body p-2">
	 						 			<h5 class="card-title">Hot Monica</h5>
	    								<p class="card-text">Director: Tayo <br/>Duration: 1.30hrs<br/>Genre: Drama, Comedy<br/>Ratings: 5.6</p>
	    								<button class="btn btn-outline-secondary btn-block rent"> Rent</button>
	 						 		</div>
								</div>
							</div>
							<div class="col-md-3 col-lg-3 mb-3">
								<div class="card">
	  								<img class="card-img-top" src="images/download (4).jpg" alt="Card image cap">
	 						 		<div class="card-body p-2">
	 						 			<h5 class="card-title">Rain of Hope II</h5>
	    								<p class="card-text">Director: Tayo <br/>Duration: 2.00hrs<br/>Genre: Comedy, Romance, Drama<br/>Ratings: 8.6</p>
	    								<button class="btn btn-outline-secondary btn-block rent"> Rent</button>
	 						 		</div>
								</div>
							</div>
							<div class="col-md-3 col-lg-3 mb-3">
								<div class="card">
	  								<img class="card-img-top" src="images/download (5).jpg" alt="Card image cap">
	 						 		<div class="card-body p-2">
	 						 			<h5 class="card-title">Heart of a maiden</h5>
	    								<p class="card-text">Director: Tayo <br/>Duration: 3.30hrs<br/>Genre‎: Romance, ‎Comedy, Drama<br/>Ratings: 9.6</p>
	    								<button class="btn btn-outline-secondary btn-block rent"> Rent</button>
	 						 		</div>
								</div>
							</div>
							<div class="col-md-3 col-lg-3 mb-3" >
								<div class="card">
			  						<img class="card-img-top" src="images/download (6).jpg" alt="Card image cap">
			 						<div class="card-body p-2">
			 						 	<h5 class="card-title">Mrs Trouble</h5>
			    						<p class="card-text">Director: Tayo <br/>Duration: 1.30hrs<br/>Genre‎: Drama, Romance, ‎Comedy<br/>Ratings: 4.0</p>
			    					<button class="btn btn-outline-secondary btn-block rent"> Rent</button>
		 						 	</div>
								</div>
							</div>
							<div class="col-md-3 col-lg-3 mb-3">
								<div class="card">
			  						<img class="card-img-top img-responsive" src="images/Knocking-On-Heavens-Door-Latest-2014-Nigerian-Nollywood-Drama-Movie-English-Full-HD.jpg" alt="Card image cap">
			 						<div class="card-body p-2">
			 						 	<h5 class="card-title">	Heavens' door</h5>
			    						<p class="card-text">Director: Tayo <br/>Duration: 2.00hrs<br/>Genre‎: Romance, Adventure, Comedy<br/>Ratings: 7.5</p>
			    						<button class="btn btn-outline-secondary btn-block rent"> Rent</button>
	 						 		</div>
								</div>
							</div>
							<p class="lead pb-3 mt-2 mx-auto">Note: Movies rented are to be returned on or before seven days after rental. </p>
						</div>
					</div>
					</div>
				</div>
			<?php  include_once ("templates/footer.php")?>